#include <iostream>

using namespace std;
//programa que calcula los caminos posibles en una cuadricula n*n
int main()
{int numerofilasycolumnas,numerocaminosposibles;
    cout << "Escriba un numero n para ver los caminos posibles en una cuadricula n*n" << endl;
    cin>>numerofilasycolumnas;
    numerocaminosposibles=(numerofilasycolumnas+1)*numerofilasycolumnas;//se calcula los caminos posibles y se imprimen
    cout<<"Para un malla "<<numerofilasycolumnas<<" * "<<numerofilasycolumnas<<" puntos hay "<<numerocaminosposibles<<" caminos."<<endl;
    return 0;
}
